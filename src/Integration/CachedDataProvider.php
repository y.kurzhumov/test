<?php
namespace Integration;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;

class CachedDataProvider implements DataProviderInterface
{
    /**
     * @var DataProviderInterface
     */
    private $provider;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Время жизни кеша в секндах
     *
     * @var int
     */
    private $cacheTtl;

    /**
     * @param DataProviderInterface $provider
     * @param CacheItemPoolInterface $cache
     * @param LoggerInterface $logger
     * @param int $cacheTtl Время жизни кеша в секндах
     */
    public function __construct(
        DataProviderInterface $provider,
        CacheItemPoolInterface $cache,
        LoggerInterface $logger,
        int $cacheTtl
    ) {
        $this->provider = $provider;
        $this->cache = $cache;
        $this->logger = $logger;
        $this->cacheTtl = $cacheTtl;
    }

    /**
     * @inheritdoc
     */
    public function get(array $request): array
    {
        try {
            $cacheKey = $this->getCacheKey($request);
            $cacheItem = $this->cache->getItem($cacheKey);
            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = $this->provider->get($request);

            $cacheItem
                ->set($request)
                ->expiresAfter($this->cacheTtl);
            $this->cache->save($cacheItem);

            return $result;

        } catch (\Exception $e) {
            $this->logger->critical(sprintf('Integration request error: %s', $e->getMessage()), [
                'exception' => $e,
            ]);

            throw new \RuntimeException('Integration request error');
        }
    }

    /**
     * @param array $request
     *
     * @return string
     */
    public function getCacheKey(array $request): string
    {
        return serialize($request);
    }
}
